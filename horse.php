<?php

const PRODUCTIONS = [
    ['x' => 2, 'y' => 1],
    ['x' => 1, 'y' => 2],
    ['x' => -1, 'y' => 2],
    ['x' => -2, 'y' => 1],
    ['x' => -2, 'y' => -1],
    ['x' => -1, 'y' => -2],
    ['x' => 1, 'y' => -2],
    ['x' => 2, 'y' => -1],
];

function init($_N, $X, $Y): void
{
    global $TRIES;
    global $BOARD;
    global $EXISTS;
    global $N;
    global $NN;

    $EXISTS = false;
    $TRIES = 0;
    $N = $_N;
    $NN = $N * $N;

    $BOARD = [];
    foreach (range(0, $N - 1) as $i) {
        $BOARD[] = [];

        foreach (range(0, $N - 1) as $j)
            $BOARD[$i][] = 0;
    }
    $BOARD[$X - 1][$Y - 1] = 1;
}

function go(int $L, int $X, int $Y): void
{
    goRecursive($L, $X - 1, $Y - 1);
}

function printTraceMessage(string $state, int $K, int $U, int $V, int $L): void
{
    global $TRIES;

    $success = str_starts_with($state, 'Laisva');
    if ($K === 8 && !$success)
        $state .= ". Backtrack";

    $Up1 = $U + 1;
    $Vp1 = $V + 1;

    echo str_pad($TRIES, 7, ' ', STR_PAD_LEFT) . ") " .
        str_repeat('-', $L - 2) .
        "R$K. U=$Up1, V=$Vp1. L=$L. $state.\n";
}

function goRecursive(int $L, int $X, int $Y): array
{
    global $TRIES;
    global $BOARD;
    global $EXISTS;
    global $N;
    global $NN;

    $K = 0;

    do {
        $TRIES++;

        $U = $X + PRODUCTIONS[$K]['x'];
        $V = $Y + PRODUCTIONS[$K]['y'];
        $K++;

        if ($U < 0 || $U >= $N || $V < 0 || $V >= $N) {
            printTraceMessage('Už krašto', $K, $U, $V, $L);
            continue;
        }

        if ($BOARD[$U][$V] !== 0) {
            printTraceMessage('Siūlas', $K, $U, $V, $L);
            continue;
        }

        $Up1 = $U + 1;
        $Vp1 = $V + 1;
        printTraceMessage("Laisva. LENTA[$Up1, $Vp1]:=$L", $K, $U, $V, $L);
        $BOARD[$U][$V] = $L;
        if ($L == $NN) {
            $EXISTS = true;
            break;
        }

        goRecursive($L + 1, $U, $V);

        if (!$EXISTS)
            $BOARD[$U][$V] = 0;
    } while (!$EXISTS && $K !== 8);

    return ['K' => $K, 'X' => $X, 'Y' => $Y];
}

function run(int $_N, int $X, int $Y, int $testNum): void
{
    global $TRIES;
    global $BOARD;
    global $EXISTS;
    global $N;

    $dirPath = __DIR__ . "/$testNum";
    $shortFilePath = $dirPath . "/out-trumpas.txt";
    $longFilePath = $dirPath . "/out-ilgas.txt";

    if (!is_dir($dirPath)) mkdir($dirPath);

    $N = $_N;
    init($N, $X, $Y);

    ob_start();
    echo "PIRMA DALIS. Duomenys\n";
    echo '    1) Lenta ' . $N . 'x' . $N . ".\n";
    echo "    2) Pradinė žirgo padėtis X=$X, Y=$Y, L=1.\n\n";
    $firstPart = ob_get_contents();
    ob_flush();

    ob_start();
    echo "ANTRA DALIS. Vykdymas\n";
    go(2, $X, $Y);
    echo "\n";
    $secondPart = ob_get_contents();
    ob_clean();

    ob_start();
    echo "TREČIA DALIS. Rezultatai\n";
    if ($EXISTS) {
        echo "  1) Apejimas rastas. Bandymų $TRIES.\n";
        echo "  2) Apejimas grafiškai:\n\n";
        echo "  Y, V ^\n";
        for ($i = $N - 1; $i >= 0; $i--) {
            $ip1 = $i + 1;
            echo "     $ip1 |";
            for ($j = 0; $j < $N; $j++)
                echo str_pad($BOARD[$i][$j], 3, ' ', STR_PAD_LEFT);
            echo "\n";
        }
        echo "       " . str_repeat('-', $N * 3 + 1) . "> X, U\n";
        echo "        ";
        foreach (range(1, $N) as $i)
            echo str_pad($i, 3, ' ', STR_PAD_LEFT);
        echo "\n\n";
    } else echo "  1) Apejimo nėra. Bandymų $TRIES.\n\n";
    $thirdPart = ob_get_contents();
    ob_flush();

    file_put_contents($shortFilePath, $firstPart . $thirdPart);
    file_put_contents($longFilePath, $firstPart . $secondPart . $thirdPart);
}

run(5, 1, 1, 1);
run(5, 5, 1, 2);
run(5, 1, 5, 3);
run(5, 2, 1, 4);
run(6, 1, 1, 5);
run(7, 1, 1, 6);
run(4, 1, 1, 7);
